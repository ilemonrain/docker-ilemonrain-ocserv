## Openconnect Server on Docker
(VPN on Docker的一次尝试)

### 0. 一点废话
镜像仍然处于开发阶段（也就是Sbeam上所谓的抢先体验版），修BUG随缘，更新新功能随缘，如果发现BUG或者更好的功能，请及时通过E-mail联系我！  
  
**长城/鹏博士宽带用户请使用SS方案！此SSTP方案不适合你们！**  
**长城/鹏博士宽带用户请使用SS方案！此SSTP方案不适合你们！**  
**长城/鹏博士宽带用户请使用SS方案！此SSTP方案不适合你们！**  
  
重要的事情说三遍！  
  
原因我会在下面写出来。  
  
### 1. 镜像说明
一次脑洞大开的尝试：让VPN也能Docker化，一键搭建VPN环境，比一键脚本还要酸爽的一种体验；集成基于Openconnect VPN Server (Ocserv) SSTP/IPsec VPN方案，可完美兼容Cisco Anyconnect SSTP/IPsec VPN (Cisco AnyConnect Secure Mobility Client)，同时支持自定义路由功能，智能分流(已加入以进行测试，**请不要用于非法活动，后果自负**)，相对于SS方案来讲，用VPN可以让更多类型流量通过(比如ICMP报文，也就是Ping包，基于Socks5的SS协议无法穿透)，同时降低延迟，更适合用来做网游加速器方案。  
  
### 2. 食用方法
启动命令行：  
  
```
docker run --cap-add NET_ADMIN --cap-add NET_RAW --device=/dev/net/tun -d -p 6666:6666/tcp -p 6666:6666/udp --name docker-ocserv ilemonrain/ocserv
```  
  
参数说明：
 > **--cap-add NET_ADMIN**：[勿动] 启动Docker TUN支持  
 > **--cap-add NET_RAW**： [勿动] 启动Docker iptables支持  
 > **--device=/dev/net/tun**： [勿动] Docker TUN支持所需的设备  
 > **-d**： 以后台模式启动(Daemon)  
 > **-p 6666:6666/tcp**：将TCP端口映射到容器内部的端口(TCP/UDP必须同时映射)  
 > **-p 6666:6666/udp**：将UDP端口映射到容器内部的端口   
 > **--name docker-ocserv**：容器名称   
 > **ilemonrain/ocserv**： [勿动] 镜像名称    
  
### 3. 为什么长城/鹏博士等宽带不能使用此镜像
小众的宽带运营商，为了降低自己网络设备的压力，会主动干扰并屏蔽所有的VPN协议，导致VPN无法正常工作。使用以上宽带的用户，建议改用基于Socks5的影梭(SS)方案。  
  
### 4. 为什么没有使用“--privileged”参数
使用“--privileged”参数，相当于整个容器完全和宿主机对接，会对宿主机造成极大的安全威胁，所以有限的暴露，会尽最大可能保护容器的安全。  

### 5. 如何使用自己的用户名/密码登录VPN
请在启动参数中继续附加参数：  
```
... 6666:6666/udp] -e ocserv_username="username" -e ocserv_userpass="password" [--name docker-ocserv ...
```  
将username和password改为你需要的用户名密码即可。  
   
### 6. 如何指定连接到VPN后使用的DNS服务器
请在启动参数中继续附加参数：  
```
... 6666:6666/udp] -e ocserv_dns="dnsserver" [--name docker-ocserv ...
```  
将dnsserver改成你需要的dns服务器即可。(推荐使用8.8.8.8)  

### 7. 更新记录
ilemonrain/ocserv Update 2018/04/25  
 > 加入ocserv-cn-no-route，进行智能分流 (但目前准确率不高) (Build 20180424) 
 > 升级ocserv核心到0.12.0  
   
### 7. 联系作者
E-mail到：ilemonrain#ilemonrain.com (看到秒回)  
Telegram：@ilemonrain (随缘在线)  