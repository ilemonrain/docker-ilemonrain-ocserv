#!/bin/sh
echo ""
echo " Starting Openconnect VPN Server on Docker ..."
echo ""
echo " [i] Running <ocserv -v> ..."
ocserv -v
echo " [i] Setting up Ocserv Config ... "
sed -i "/max-clients/s/16/${ocserv_max_clients}/" /etc/ocserv/ocserv.conf
sed -i "/max-same-clients/s/2/${ocserv_max_same_clients}/" /etc/ocserv/ocserv.conf
sed -i "/tcp-port/s/443/${ocserv_tcp_port}/" /etc/ocserv/ocserv.conf
sed -i "/udp-port/s/443/${ocserv_udp_port}/" /etc/ocserv/ocserv.conf
sed -i "/try-mtu-discovery/s/false/${ocserv_try_mtu_discovery}/" /etc/ocserv/ocserv.conf
sed -i "/dns = 192/s/192.168.1.2/${ocserv_dns}/" /etc/ocserv/ocserv.conf
sed -i "/cisco-client-compat/s/false/${ocserv_cisco_client_compat}/" /etc/ocserv/ocserv.conf
sed -i "/auth/s/.\/sample.passwd/\/etc\/ocserv\/ocpasswd/" /etc/ocserv/ocserv.conf
sed -i "/no-route = 192/s/no-route/#no-route/" /etc/ocserv/ocserv.conf
sed -i "/route = 10/s/route/#route/" /etc/ocserv/ocserv.conf
sed -i "/route = 192/s/route/#route/" /etc/ocserv/ocserv.conf
sed -i "/ipv4-network/s/192.168.1.0/10.233.66.0/g" /etc/ocserv/ocserv.conf
sed -i "/server-cert/s/..\/tests\/certs\/server-cert.pem/\/etc\/ocserv\/certs\/server-cert.pem/" /etc/ocserv/ocserv.conf
sed -i "/server-key/s/..\/tests\/certs\/server-key.pem/\/etc\/ocserv\/certs\/server-key.pem/" /etc/ocserv/ocserv.conf
echo " [i] Importing Ocserv CHN no-route policy ..."
cat /etc/ocserv/chn-no-route.txt >> /etc/ocserv/ocserv.conf
echo " [i] Setting up ocserv user : ${ocserv_username} ..."
echo "${ocserv_userpass}" >> _tmp_ocpasswd
echo "${ocserv_userpass}" >> _tmp_ocpasswd
ocpasswd -c /etc/ocserv/ocpasswd ${ocserv_username} < _tmp_ocpasswd
rm -f _tmp_ocpasswd
echo " [i] Staring ocserv ... "
echo ""
ocserv -c /etc/ocserv/ocserv.conf -f -d 1